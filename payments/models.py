from django.db import models
from django.utils.translation import gettext as _

from orgreg.models import User

def default_membership_fee():
    return int(Option.get('membership_fee', '0'), 10)

# Create your models here.
class UserMembeship(models.Model):
    user = models.OneToOneField('orgreg.User', on_delete=models.CASCADE)
    membership_fee = models.PositiveIntegerField(verbose_name=_('Membership fee'), default=default_membership_fee)

class Payment(models.Model):
    user = models.ForeignKey(User, related_name='payments', on_delete=models.CASCADE)
    year = models.PositiveIntegerField()
    # The transaction's full name, based on its type's name.
    # Longer, becaues it will usually be "{{type.name}} {{year}}"
    name = models.CharField(max_length=200)
    value = models.PositiveIntegerField()
    paid = models.PositiveIntegerField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    
    def __str__(self):
        return "{} {} {} {}".format(self.year, self.user.first_name, self.user.last_name, self.paid)

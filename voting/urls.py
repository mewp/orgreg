from django.urls import path
from . import views

app_name='voting'
urlpatterns = [
    path('resolutions/', views.index, name='resolutions'),
    path('resolution/<int:id>/start_vote', views.start_vote, name='start_vote'),
    path('resolution/<int:id>/end_vote', views.end_vote, name='end_vote'),
    path('resolution/<int:id>/vote/yea', views.vote_yea, name='vote_yea'),
    path('resolution/<int:id>/vote/nay', views.vote_nay, name='vote_nay'),
    path('resolution/<int:id>/vote/abstain', views.vote_abstain, name='vote_abstain'),
    path('resolution/<int:id>/vote/rescind', views.vote_rescind, name='vote_rescind'),
]

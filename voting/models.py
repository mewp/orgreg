from datetime import date

from django.db import models, transaction
from orgreg.models import User, Event, Option
from django.contrib.auth.models import Group

from orgreg.models import Event

class ResolutionType(models.Model):
    name = models.TextField(unique=True)
    committee = models.ForeignKey(Group, on_delete=models.SET_NULL, null=True)
    leader = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    
    only_leader_manages_votes = models.BooleanField(default=False)
    vote_duration = models.DurationField(null=True, blank=True)
    minimum_positive_votes_percentage = models.PositiveSmallIntegerField(null=True, blank=True)
    majority_percentage = models.PositiveSmallIntegerField(null=True, blank=True)

    vote_ends_when_outcome_certain = models.BooleanField(default=False)
    vote_ends_when_all_votes_cast = models.BooleanField(default=False)

    def __str__(self):
        return self.name

class Resolution(models.Model):
    PENDING = 1
    VOTING = 2
    APPROVED = 3
    REJECTED = 4
    STATUS_CHOICES = (
        (PENDING, 'Pending'),
        (VOTING, 'Voting'),
        (APPROVED, 'Approved'),
        (REJECTED, 'Rejected'),
    )

    number = models.PositiveIntegerField(blank=True, null=True)
    type = models.ForeignKey(ResolutionType, on_delete=models.PROTECT, null=True)
    series = models.CharField(max_length=64, blank=True, null=True)
    title = models.TextField()
    content = models.TextField()
    status = models.PositiveSmallIntegerField(choices=STATUS_CHOICES, default=PENDING)
    metadata = models.JSONField(blank=True, null=True)

    @transaction.atomic 
    def open(self):
        self.status = Resolution.PENDING
        self.save()
        Event.emit('resolution_open', self.id)

    @transaction.atomic 
    def resolve(self):
        positive_votes = self.votes.filter(decision=Vote.APPROVE).count()
        negative_votes = self.votes.filter(decision=Vote.APPROVE).count()
        voters = self.type.committee.user_set.count()
        decision = True
        minimum_positive_votes = self.type.minimum_positive_votes_percentage
        if minimum_positive_votes is not None and 100*positive_votes/voters < minimum_positive_votes:
            decision = False

        majority_percentage = self.type.majority_percentage
        if majority_percentage is not None and 100*positive_votes/(positive_votes+negative_votes) < majority_percentage:
            decision = False
        
        if decision:
            self.status = Resolution.APPROVED
            self.series = Option.get('voting_current_resolution_series', date.today().year)
            self.number = Resolution.objects.filter(series=self.series).order_by('-number').first() or 1
            Event.emit('resolution_approved', self.id)
        else:
            self.status = Resolution.REJECTED
            Event.emit('resolution_rejected', self.id)
        self.save()

    def __str__(self):
        return "{}: {}".format(self.type.name, self.title)

class Vote(models.Model):
    APPROVE = 1
    REJECT = 2
    ABSTAIN = 3

    DECISION_CHOICES = {
        APPROVE: 'Yea',
        REJECT: 'Nay',
        ABSTAIN: 'Abstain',
    }

    resolution = models.ForeignKey(Resolution, on_delete=models.CASCADE, related_name='votes')
    voter = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name='votes')
    decision = models.PositiveSmallIntegerField(choices=DECISION_CHOICES.items())

    class Meta:
        unique_together = (('resolution', 'voter'),)

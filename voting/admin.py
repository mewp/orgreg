from django.contrib import admin
from orgreg.admin import admin_site
from .models import ResolutionType, Resolution

admin_site.register(ResolutionType)
admin_site.register(Resolution)

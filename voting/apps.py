from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _
from orgreg.plugin import Plugin

class VotingConfig(AppConfig, Plugin):
    name = 'voting'

    menu_items = (
        (_('Resolutions'), 'resolutions'),
    )

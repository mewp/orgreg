from django.views.decorators.http import require_POST
from django.shortcuts import render, get_object_or_404, redirect
from django.core.exceptions import PermissionDenied
from django.contrib.auth.decorators import login_required
from .models import Resolution, Vote

@login_required
def index(request):
    resolutions = Resolution.objects.all().order_by('-id')
    user_votes = {
        id: Vote.DECISION_CHOICES[decision]
        for (id, decision) in request.user.votes.values_list('resolution__id', 'decision')
    }
    return render(request, 'resolutions.html.jinja', {
        'resolutions': resolutions,
        'votes': user_votes
    })

@require_POST
def start_vote(request, id):
    resolution = get_object_or_404(Resolution, id=id)
    if not request.user.groups.filter(id=resolution.type.committee.id).exists():
        raise PermissionDenied
    if resolution.status != Resolution.PENDING:
        raise PermissionDenied("You can't start a vote an a resolution that isn't pending. ")
    resolution.status = Resolution.VOTING
    resolution.save()
    return redirect('voting:resolutions')

@require_POST
def vote(request, id, decision):
    resolution = get_object_or_404(Resolution, id=id)
    if not request.user.groups.filter(id=resolution.type.committee.id).exists():
        raise PermissionDenied
    if resolution.status != Resolution.VOTING or request.user.votes.filter(resolution=resolution).exists():
        raise PermissionDenied
    Vote.objects.create(voter=request.user, decision=decision, resolution=resolution)
    return redirect('voting:resolutions')

def vote_yea(request, id):
    return vote(request, id, Vote.APPROVE)

def vote_nay(request, id):
    return vote(request, id, Vote.REJECT)

def vote_abstain(request, id):
    return vote(request, id, Vote.ABSTAIN)

@require_POST
def vote_rescind(request, id):
    resolution = get_object_or_404(Resolution, id=id)
    if not request.user.groups.filter(id=resolution.type.committee.id).exists():
        raise PermissionDenied
    vote = get_object_or_404(Vote, resolution=resolution, voter=request.user)
    if resolution.status != Resolution.VOTING:
        raise PermissionDenied
    vote.delete()
    return redirect('voting:resolutions')

@require_POST
def end_vote(request, id):
    resolution = get_object_or_404(Resolution, id=id)
    if not request.user.groups.filter(id=resolution.type.committee.id).exists():
        raise PermissionDenied
    if resolution.status != Resolution.VOTING:
        raise PermissionDenied
    resolution.resolve()
    resolution.save()
    return redirect('voting:resolutions')

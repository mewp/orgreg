from django.conf import settings, urls

class Plugin:
    """A marker class for orgreg plugins."""
    pass

def plugins():
    from django.apps import apps
    return (app for app in apps.get_app_configs() if isinstance(app, Plugin))

def get_urls():
    for plugin in plugins():
        yield urls.include(plugin.name + '.urls', namespace=plugin.name)

def get_menu_items():
    for plugin in plugins():
        if hasattr(plugin, 'menu_items'):
            for (label, name) in plugin.menu_items:
                yield (label, plugin.name + ':' + name)

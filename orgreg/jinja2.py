from jinja2 import nodes
from jinja2.ext import Extension
from django.template import Template, RequestContext

class DjangoExtends(Extension):
    tags = {'django_extends'}

    def parse(self, parser):
        lineno = next(parser.stream).lineno
        template_name = parser.parse_expression()

        end = parser.stream.current
        parser.stream.push(end)

        parser.stream.expect('block_end')

        template = '{{% extends "{}" %}}'.format(template_name.value)

        args = [nodes.MarkSafe(nodes.Const(template)), nodes.ContextReference()]
        bodies = []
        while not parser.stream.look().test('eof'):
            parser.stream.expect('block_end')
            parser.stream.next_if('data')
            parser.stream.expect('block_begin')
            parser.stream.expect('name:block')
            block_name = nodes.Const(parser.parse_expression().name)
            body = parser.parse_statements(['name:endblock'], drop_needle=True)
            
            bodies.append(nodes.CallBlock(self.call_method('_render_block', [block_name]), [], [], body))

        return nodes.CallBlock(self.call_method('_render_template', args), [], [], bodies)

    def _render_template(self, template, context, caller):
        return Template(template + caller()).render(RequestContext(context.get('request'), context.parent))

    def _render_block(self, name, caller):
        return '{{% block {} %}}{}{{% endblock %}}'.format(name, caller())

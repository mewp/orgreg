from django.apps import AppConfig
class OrgregConfig(AppConfig):
    name = 'orgreg'

    def ready(self):
        from django.contrib.admin import autodiscover
        autodiscover()

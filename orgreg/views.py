from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.forms import SetPasswordForm, PasswordChangeForm
from social_django.models import UserSocialAuth
from django.utils.translation import gettext as _
from django.http import HttpResponse, HttpResponseForbidden
from django.template import Template, Context
from .forms import RegistrationForm, SocialRegistrationForm
from .models import User, Option
from django.conf import settings

@login_required
def index(request):
    profile_data = None
    if 'username' in request.POST:
        profile_data = request.POST
    profile_form = SocialRegistrationForm(profile_data, instance=request.user)
    if profile_data is not None and profile_form.is_valid():
        profile_form.save()

    user = User.objects.get(id=request.user.id)

    # Change password form
    password_data = None
    if 'new_password1' in request.POST:
        password_data = request.POST
    if request.user.password == '':
        password_form = SetPasswordForm(request.user, password_data)
    else:
        password_form = PasswordChangeForm(request.user, password_data)

    if password_data is not None and password_form.is_valid():
        password_form.save() 

    unpaid_fees = request.user.payments.filter(paid=None) 
    paid_fees = request.user.payments.exclude(paid=None) 

    return render(request, 'index.html', {
        'profile_form': profile_form,
        'password_form': password_form,
        'unpaid_fees': unpaid_fees,
        'paid_fees': paid_fees,
        'currency': Option.get('currency'),
        'google_auth_enabled': settings.GOOGLE_AUTH_ENABLED,
        'user': user,
    })

def register(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('index')
    else: 
        form = RegistrationForm()
    return render(request, 'register.html', {'form': form})

@staff_member_required
def generate_payments(request):
   payments = []
   year = request.POST['year']
   for user in User.objects.filter(is_member=True):
       p = Payment.objects.get_or_create(user=user, year=int(year, 10), defaults={
           'value': user.membership_fee,
           'name': _('Membership fee %d') % (2017,),
       })[0]
       payments.append('{},{},{},{},{}'.format(p.user.first_name, p.user.last_name, p.name, p.value, p.paid or 0))
   return HttpResponse('\n'.join(payments), content_type='text/plain; charset=utf-8')

@staff_member_required
def import_payments(request):
   if request.method == 'POST':
       data = list(line.split(',') for line in request.POST['data'].split('\n'))
       for line in data:
           try:
               payment = Payment.objects.get(user__first_name=line[0], user__last_name=line[1], name=line[2])
               payment.paid = int(line[3], 10)
               payment.save()
           except Payment.DoesNotExist:
               print("{} {} {} not found".format(line[0], line[1], line[2]))
               pass

   return render(request, 'import_payments.html')

@login_required
def pay(request, id):
   payment = get_object_or_404(Payment, id=id)
   if payment.user != request.user:
       return HttpForbidden('403 Forbidden')

   transfer_data = Template(Option.get('bank_transfer_template')).render(Context({'title': payment.name, 'amount': payment.value}))
   return render(request, 'pay.html', {'transfer_data': transfer_data})

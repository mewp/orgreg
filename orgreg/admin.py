import urllib
from django.contrib import admin
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.admin.views.decorators import staff_member_required
from django.http import HttpResponseForbidden, HttpResponseRedirect
from django.urls import reverse
from django.utils.module_loading import autodiscover_modules
from django.utils.translation import gettext as _
from .models import User, Option

class AdminSite(admin.AdminSite):
    index_template = 'admin/custom_index.html'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._registry.update(admin.site._registry)

    def _is_login_redirect(self, response):
        if isinstance(response, HttpResponseRedirect):
            login_url = reverse('admin:login', current_app=self.name)
            response_url = urllib.parse.urlparse(response.url).path
            return login_url == response_url
        else:
            return False

    def admin_view(self, view, cacheable=False):
        inner = super().admin_view(view, cacheable)

        def wrapper(request, *args, **kwargs):
            response = inner(request, *args, **kwargs)
            if self._is_login_redirect(response):
                return HttpResponseForbidden('')
            else:
                return response

        return wrapper

class UserAdmin(BaseUserAdmin):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        abstract_fields = [field.name for field in AbstractUser._meta.fields]
        abstract_fields += ['created_at', 'updated_at']
        user_fields = [field.name for field in self.model._meta.fields]

        self.fieldsets += (
            (_('Extra fields'), {
                'fields': [
                    f for f in user_fields if (
                        f not in abstract_fields and
                        f != self.model._meta.pk.name
                    )
                ],
            }),
        )

admin_site = AdminSite()
admin_site.register(User, UserAdmin)
admin_site.register(Option)
admin_site.login = lambda request: HttpResponseForbidden('')

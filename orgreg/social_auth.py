from django.shortcuts import render
from django.contrib.auth.models import User
from social_core.pipeline.partial import partial

from .forms import SocialRegistrationForm

# partial says "we may interrupt, but we will come back here again"
@partial
def register_user(strategy, backend, request, details, *args, **kwargs):
    if kwargs['user']:
        return

    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            return {'is_new': True, 'user': form.save()}
    else:
        form = SocialRegistrationForm(details)

    return render(request, 'register_social.html', {'form': form})

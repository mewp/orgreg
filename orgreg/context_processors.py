from django.conf import settings
from .plugin import get_menu_items

def export_settings(request):
    return {'settings': settings}

def menu_items(requesst):
    return {'menu_items': list(get_menu_items())}

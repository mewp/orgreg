"""orgreg URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.urls import path, include
from django.contrib import admin
from django.contrib.auth.views import LoginView
from django.conf import settings

from . import views, plugin
from .admin import admin_site

urlpatterns = [
    path('', views.index, name='index'),
    path('login/', LoginView.as_view(extra_context={'google_auth_enabled': settings.GOOGLE_AUTH_ENABLED})),
    path('register/', views.register, name='register'),
    path('admin/', admin_site.urls),
    #url(r'^generate_payments/$', views.generate_payments, name='generate_payments'),
    #url(r'^pay/(?P<id>\d+)/$', views.pay, name='pay'),
    #url(r'^import_payments/$', views.import_payments, name='import_payments'),
    path('', include('django.contrib.auth.urls')),
]

if settings.GOOGLE_AUTH_ENABLED:
    urlpatterns.append(url('', include('social_django.urls', namespace='social')))

for patterns in plugin.get_urls():
    urlpatterns.append(path('', patterns))

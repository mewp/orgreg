import json
import django
from django.core import serializers
from django.db import models
from django.db.models import Exists, OuterRef
from django import forms
from django.contrib.auth.models import User
from django.utils.translation import gettext_lazy as _
from django.db.models.signals import post_save
from django.dispatch import receiver, Signal

class Option(models.Model):
    name = models.CharField(max_length=64, unique=True)
    value = models.TextField()

    @classmethod
    def get(cls, name, default=None):
        try:
            return cls.objects.get(name=name).value
        except django.db.utils.OperationalError:
            # Most likely the table doesn't exist yet, let's ignore that.
            return default 
        except cls.DoesNotExist:
            return default 

    def __str__(self):
        return self.name

class User(User):
    class Meta:
        proxy = True

    def is_member(self):
        print(self.id)
        return Event.objects.annotate(
            ended=Exists(Event.objects.filter(
                event_type='user_membership_end',
                object_id=self.id,
                created_at__gt=OuterRef('created_at')
            ))
        ).filter(event_type='user_membership_start', object_id=self.id, ended=False).exists()

event = Signal()

class Event(models.Model):
    event_type = models.TextField(db_index=True)
    object_id = models.PositiveIntegerField(db_index=True)
    data = models.JSONField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)

    @classmethod
    def emit(cls, event_type, object_id, data=None):
        cls.objects.create(event_type=event_type, object_id=object_id, data=data)
        event.send(sender=event_type, object_id=object_id, data=data)

@receiver(post_save)
def log_change(sender, instance, created, raw, using, update_fields, **kwargs):
    if raw or sender in (Event,) \
    or sender.__module__ not in (__name__, 'django.contrib.auth.models'):
        return
    data = json.loads(serializers.serialize('json', [instance]))[0]['fields']
    if created:
        Event.emit('create_' + sender._meta.model_name, instance.pk, data)
    # TODO: Emit update events

# TODO: Emit delete events

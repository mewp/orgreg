from django.db import models
from django.dispatch import receiver
from orgreg.models import event, Event
from voting.models import Resolution, ResolutionType

@receiver(event)
def user_created(sender, object_id, data, **kwargs):
    if sender != 'create_user':
        return
    Resolution.objects.create(
        type=ResolutionType.objects.get(name='Uchwała zarządu'),
        title='Przyjęcie członka: {} {}'.format(data['first_name'], data['last_name']),
        content='Z dniem ogłoszenia tej ustawy, {} {} zostaje wpisany na listę członków zwyczajnych Polskiej Grupy Użytkowników Linuksa.'.format(data['first_name'], data['last_name']),
        metadata={
            'type': 'add_member',
            'user_id': object_id
        }
    )

@receiver(event)
def resolution_approved(sender, object_id, data, **kwargs):
    if sender != 'resolution_approved':
        return
    resolution = Resolution.objects.get(id=object_id)

    if resolution.metadata.get('type', None) != 'add_member':
        return

    Event.emit('user_membership_start', resolution.metadata['user_id'])
